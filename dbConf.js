var mongoose = require('mongoose');

var dbUri = "mongodb://127.0.0.1:27017/rest";

//creacion de la conexion a la bd
mongoose.connect(dbUri);

//eventos de conexion
mongoose.connection.on('connected', function(){
	console.log('Conectado a mongo: ' + dbUri);
});


mongoose.connection.on('error', function(){
	console.log('Error al conectar a mongo: ' + dbUri);
});

mongoose.connection.on('disconnected', function(){
	console.log('Ups se ha desconectado de mongo: ' + dbUri);
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() { 
  mongoose.connection.close(function () { 
    console.log('Mongoose default connection disconnected through app termination'); 
    process.exit(0); 
  });
}); 

module.exports = mongoose;